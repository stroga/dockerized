import React from 'react';
import { shallow } from 'enzyme';

import Artists from './Artists';

describe('Artists', () => {
  it('should be rendered properly', () => {
    const ArtistsComponent = shallow(<Artists />);
    expect(ArtistsComponent.html())
      .toEqual('<h1>MTV Base Headline Artists 2019</h1>')
  });
});
