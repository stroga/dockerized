import React, { useEffect, useState } from 'react';
import './App.scss';

export default function Artists() {
    const [artists, setArtists] = useState([]);

    useEffect(() => {
      fetch('http://localhost:3000/api/artists').then(r => r.json()).then(res => setArtists(res));
    }, []);

    return (
        <>
            <h1>MTV Base Headline Artists 2019</h1>
            {artists.map(artist => (
                <div id="card-body" key={artist.id}>
                    <div className="card">
                        <h2>{artist.name}</h2>
                        <p>genre: {artist.genre}</p>
                        <p>Albums released: {artist.albums}</p>
                    </div>
                </div>
            ))}
        </>
    );
}
