const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = process.env.PORT || 3000;

const artists = [
  {
    id: '1',
    name: 'Davido',
    country: 'Nigeria',
    genre: 'Afro-Pop',
    albums: '2',
  },
  {
    id: '2',
    name: 'AKA',
    country: 'South-Africa',
    genre: 'Hip-Hop',
    albums: '4',
  },
  {
    id: '3',
    name: 'Seyi Shay',
    country: 'Nigeria',
    genre: 'R&B',
    albums: '2',
  },
  {
    id: '4',
    name: 'Sauti Sol',
    country: 'Kenya',
    genre: 'Soul',
    albums: '3',
  },
  {
    id: '5',
    name: 'Davido',
    country: 'Nigeria',
    genre: 'Afro-Pop',
    albums: '2',
  },
  {
    id: '6',
    name: 'AKA',
    country: 'South-Africa',
    genre: 'Hip-Hop',
    albums: '4',
  },
  {
    id: '7',
    name: 'Seyi Shay',
    country: 'Nigeria',
    genre: 'R&B',
    albums: '2',
  },
  {
    id: '8',
    name: 'Sauti Sol',
    country: 'Kenya',
    genre: 'Soul',
    albums: '3',
  },
  {
    id: '9',
    name: 'Davido',
    country: 'Nigeria',
    genre: 'Afro-Pop',
    albums: '2',
  },
  {
    id: '10',
    name: 'AKA',
    country: 'South-Africa',
    genre: 'Hip-Hop',
    albums: '4',
  },
  {
    id: '11',
    name: 'Seyi Shay',
    country: 'Nigeria',
    genre: 'R&B',
    albums: '2',
  },
  {
    id: '12',
    name: 'Sauti Sol',
    country: 'Kenya',
    genre: 'Soul',
    albums: '3',
  },
  {
    id: '13',
    name: 'Davido',
    country: 'Nigeria',
    genre: 'Afro-Pop',
    albums: '2',
  },
  {
    id: '14',
    name: 'AKA',
    country: 'South-Africa',
    genre: 'Hip-Hop',
    albums: '4',
  },
  {
    id: '15',
    name: 'Seyi Shay',
    country: 'Nigeria',
    genre: 'R&B',
    albums: '2',
  },
  {
    id: '16',
    name: 'Sauti Sol',
    country: 'Kenya',
    genre: 'Soul',
    albums: '3',
  },
];

app.use(bodyParser.json());
app.use(express.static(__dirname + '/build'));

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/build/index.html');
});

app.get('/api/artists', function(req, res) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(artists));
});

app.listen(port, () => console.log(`app is running on the port: ${port}`));