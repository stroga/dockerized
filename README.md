# Simple react boilerplate

## Client side rendering application

- webpack 4
- React 16.13
- SASS
- ESLint
- Prettier
- Babel

## Up and running locally dev
`docker-compose up -d --build`

## Up and running locally prod
`docker-compose -f docker-compose.prod.yml up -d --build`