const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
module.exports = {
  mode: "development",
  output: {
    filename: 'bundle.js',
    publicPath: '/',
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[name].css',
    }),
  ],
  devServer: {
    contentBase: './browser',
    hot: true,
    host: '0.0.0.0', // Docker fix https://github.com/microsoft/vscode-remote-release/issues/1009
    port: 8080
  },
  devtool: 'source-map',
};
