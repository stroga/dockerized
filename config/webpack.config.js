const { merge } = require('webpack-merge');

const commonConfig = require('./webpack.common.config');

module.exports = (env) => {
    const config = require('./webpack.config.' + env);
    return merge(commonConfig, config);
};